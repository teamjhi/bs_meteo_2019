﻿<?php
Class Piste EXTENDS Projet{

    // PARAMETERS
    private $id;
    private $nom;
    private $prenom;
    private $email;
    private $password;

    // METHOD: Various
    public function __construct($id = null) {

        parent::__construct();

        if($id){
            $this->set_id($id);
            $this->init();
        }

    }

    public function init() {

        $query = "SELECT * FROM t_personnes WHERE id_per=:id_per";
        try {

            $stmt = $this->pdo->prepare($query);
            $args['id_per'] = $this->get_id();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_nom($tab['nom_per']);
            $this->set_prenom($tab['prenom_per']);
            $this->set_email($tab['email_per']);
            $this->set_password($tab['password_per']);
            return true;
        } catch (Exception $e) {

            return false;
        }
        return true;
    }

    public function __toString() {

        $str = "\n<pre>\n";
        foreach($this as $key => $val){
            if($key != "pdo"){
                $str .= "\t" . $key;
                $lengh_key = strlen($key);
                for($i = $lengh_key; $i < 20;$i++) {
                    $str .= "&nbsp;";
                }
                $str .= "=>&nbsp;&nbsp;&nbsp;".$val."\n";
            }
        }
        $str .= "\n</pre>";
        return $str;
    }

    public function add($tab){

        $this->gen_password($tab["password"]);

        // Tableau d'arguments
        $args['nom_per'] = $tab['nom_per'];
        $args['prenom_per'] = $tab['prenom_per'];
        $args['email_per'] = $tab['email_per'];
        $args['password_per'] = $this->get_password();

        // Requête
        $query = "INSERT INTO t_personnes SET "
            . "nom_per = :nom_per, "
            . "prenom_per = :prenom_per, "
            . "email_per = :email_per, "
            . "password_per = :password_per";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return $this->pdo->lastInsertId();

        } catch (Exception $e) {

            echo $e;
            return false;
        }
    }


    // Verifie si un email existe dans la base de données
    public function check_email($email){

        $query = "SELECT * FROM t_personnes WHERE email_per = :email LIMIT 1";

        try {

            $stmt = $this->pdo->prepare($query);
            $args[':email'] = $email;
            $stmt->execute($args);
            $tab = $stmt->fetch();
            if(strtolower($tab['email_per']) == strtolower($email)){

                return true;
            }else {

                return false;
            }

        } catch (Exception $e) {

            return false;
        }
    }

    public function check_login($email, $password){

        $query = "SELECT id_per, password_per FROM t_personnes WHERE email_per=:email LIMIT 1";

        try {

            $stmt = $this->pdo->prepare($query);
            $args[':email'] = $email;
            $stmt->execute($args);
            $tab = $stmt->fetch();
            if(password_verify($password, $tab['password_per'])){

                $_SESSION['id'] = $tab['id_per'];
                $user_browser_ip = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'];
                $_SESSION['login_string'] = password_hash($tab['password_per'] . $user_browser_ip, PASSWORD_DEFAULT);
                $_SESSION['email'] = $email;

                return true;
            }else {

                return false;
            }

        } catch(Exception $e) {

            return false;
        }

        return false;
    }

    public function check_connect(){

        if(isset($_SESSION['id'], $_SESSION['email'], $_SESSION['login_string'])) {

            $user_browser_ip = $_SERVER['HTTP_USER_AGENT'] . $_SERVER['REMOTE_ADDR'];

            if(password_verify($this->get_password() . $user_browser_ip, $_SESSION['login_string'])) {

                return true;
            }else {

                return false;
            }

        }else {

            return false;
        }

    }

    public function get_all($order = "nom_per, prenom_per") {

        $args[':order'] = $order;
        $query = "SELECT * FROM t_personnes ORDER BY :order";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetchAll();
            return($tab);

        } catch(Exception $e) {

            return false;
        }
    }

    public function get_all_aut() {

        $query = "SELECT code_aut FROM t_aut_fnc AUF "
            ."JOIN t_autorisations AUT ON AUF.id_aut=AUT.id_aut "
            ."JOIN t_fonctions FNC ON AUF.id_fnc=FNC.id_fnc "
            ."JOIN t_fnc_per FNP ON FNC.id_fnc=FNP.id_fnc AND FNP.id_per=:id_per";

        $args['id_per'] = $this->get_id();

        $stmt = $this->pdo->prepare($query);
        $stmt->execute($args);
        $tab = $stmt->fetchAll();

        foreach($tab AS $aut) {

            $tab_aut[] = $aut['code_aut'];
        }

        return $tab_aut;
    }

    public function get_all_fnc() {

        $query = "SELECT abr_fnc, nom_fnc FROM t_fonctions FNC "
            ."JOIN t_fnc_per FNP ON FNC.id_fnc=FNP.id_fnc AND FNP.id_per=:id_per ";
        try {

            $args['id_per'] = $this->get_id();
            $stmt = $this->pdo->prepare($query);

            if($stmt->execute($args)) {

                $tab_aut = $stmt->fetchAll();
                return $tab_aut;
            }else {

                return false;
            }

        } catch (Exception $e) {

            return false;
        }
    }

    public function check_aut($aut_list) {

        $tab_aut = explode(";", $aut_list);
        $tab_aut_per = $this->get_all_aut();

        foreach ($tab_aut AS $aut) {
            if(in_array($aut, $tab_aut_per)) {

                return true;
            }
        }

        return false;
    }

    public function add_fnc($id_fnc) {

        $id_per = $this->get_id();

        // Tableau d'arguments
        $args['id_per'] = $id_per;
        $args['id_fnc'] = $id_fnc;

        // Requête
        $query = "INSERT INTO t_fnc_per SET "
            . "id_per = :id_per, "
            . "id_fnc = :id_fnc";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;

        } catch (Exception $e) {

            echo $e;
            return false;
        }

    }

    public function del_fnc($id_fnc) {

        $id_per = $this->get_id();

        // Tableau d'arguments
        $args['id_per'] = $id_per;
        $args['id_fnc'] = $id_fnc;

        // Requête
        $query = "DELETE FROM t_fnc_per WHERE "
            . "id_per = :id_per AND "
            . "id_fnc = :id_fnc";

        try {

            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            return true;

        } catch (Exception $e) {

            echo $e;
            return false;
        }

    }

    public function gen_password($password) {

        $this->set_password(password_hash($password, PASSWORD_DEFAULT));
    }

    // METHOD: Setter & Getter
    public function set_id($id) {
        $this->id = $id;
    }
    public function get_id() {
        return $this->id;
    }

    public function set_nom($nom) {
        $this->nom = $nom;
    }
    public function get_nom() {
        return $this->nom;
    }

    public function set_prenom($prenom) {
        $this->prenom = $prenom;
    }
    public function get_prenom() {
        return $this->prenom;
    }

    public function set_email($email) {
        $this->email = $email;
    }
    public function get_email() {
        return $this->email;
    }

    public function set_password($password) {
        $this->password = $password;
    }
    public function get_password() {
        return $this->password;
    }

}
?>