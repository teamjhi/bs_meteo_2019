﻿<?php

/**
 * Description of Neige
 *
 * @author CP-16LEG
 */
class Neige EXTENDS Projet {
    private $id_neige;
    private $etat_neige;
    
    //Constructeur
    public function __construct($id = null) {
        parent::__construct();

        if ($id) {
            $this->set_id_meteo($id);
            $this->init();
        }
    }
    
    //Initialisation
    public function init() {
        $query = "SELECT * FROM t_neige WHERE id_neige = :id_neige";
        try {
            $stmt = $this->pdo->prepare($query);
            $args['id_neige'] = $this->get_id_neige();
            $stmt->execute($args);
            $tab = $stmt->fetch();

            $this->set_etat_neige($tab['etat_neige']);        

            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    function get_all() {

        $query = "SELECT * FROM t_neige";

        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $tab = $stmt->fetchAll();
            return($tab);
        } catch (Exception $e) {
            return false;
        }
    }
    
    function get_id_neige_by_name($neige){
        $query = "SELECT id_neige FROM t_neige WHERE etat_neige = :neige";
        $args['neige'] = $neige;
        try {
            $stmt = $this->pdo->prepare($query);
            $stmt->execute($args);
            $tab = $stmt->fetch();
            return($tab['id_neige']);
        } catch (Exception $e) {
            return false;
        }
    }
    
    function get_id_neige() {
        return $this->id_neige;
    }

    function get_etat_neige() {
        return $this->etat_neige;
    }

    function set_id_neige($id_neige) {
        $this->id_neige = $id_neige;
    }

    function set_etat_neige($etat_neige) {
        $this->etat_neige = $etat_neige;
    }


    
}
