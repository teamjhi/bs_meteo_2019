-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 04 juil. 2019 à 15:12
-- Version du serveur :  10.1.37-MariaDB
-- Version de PHP :  7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bs_meteo_2019`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_autorisations`
--

CREATE TABLE `t_autorisations` (
  `id_aut` int(10) UNSIGNED NOT NULL,
  `nom_aut` varchar(100) NOT NULL,
  `code_aut` varchar(10) NOT NULL,
  `desc_aut` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_autorisations`
--

INSERT INTO `t_autorisations` (`id_aut`, `nom_aut`, `code_aut`, `desc_aut`) VALUES
(1, 'Utilisateurs', 'ADM_USR', 'Gestion des utilisateurs'),
(2, 'Utilisateurs', 'USR_USR', 'Visualisation des utilisateurs'),
(3, 'Fonctions - Administateur des fonctions', 'ADM_FNC', 'Administateur des fonctions'),
(4, 'Autorisations - Administrations des autorisations', 'ADM_AUT', 'Administrations des autorisations'),
(5, 'Météo', 'ADM_MTO', 'Gestion de la météo et des éléments composants les formulaires'),
(6, 'Météo', 'USR_MTO', 'Saisie de la météo');

-- --------------------------------------------------------

--
-- Structure de la table `t_aut_fnc`
--

CREATE TABLE `t_aut_fnc` (
  `id_aut` int(10) UNSIGNED NOT NULL,
  `id_fnc` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_aut_fnc`
--

INSERT INTO `t_aut_fnc` (`id_aut`, `id_fnc`) VALUES
(1, 1),
(2, 1),
(2, 2),
(3, 1),
(3, 2),
(4, 1),
(5, 1),
(6, 3);

-- --------------------------------------------------------

--
-- Structure de la table `t_etat_meteo`
--

CREATE TABLE `t_etat_meteo` (
  `id_etat_meteo` int(10) UNSIGNED NOT NULL,
  `nom_etat_meteo` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_etat_meteo`
--

INSERT INTO `t_etat_meteo` (`id_etat_meteo`, `nom_etat_meteo`) VALUES
(1, 'Pluie'),
(2, 'Neige'),
(3, 'Nuageux'),
(4, 'Brouillard'),
(5, 'Peu nuageux'),
(11, 'Nuageux avec éclaircies'),
(13, 'Brouillard très bas');

-- --------------------------------------------------------

--
-- Structure de la table `t_etat_neige`
--

CREATE TABLE `t_etat_neige` (
  `id_etat_neige` int(10) UNSIGNED NOT NULL,
  `nom_etat_neige` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_etat_neige`
--

INSERT INTO `t_etat_neige` (`id_etat_neige`, `nom_etat_neige`) VALUES
(1, 'Mouillé'),
(2, 'Fine'),
(3, 'Poudreuse'),
(4, 'Collante'),
(5, 'Volumineuse'),
(6, 'Dure'),
(7, 'Cartonneuse');

-- --------------------------------------------------------

--
-- Structure de la table `t_etat_piste`
--

CREATE TABLE `t_etat_piste` (
  `id_etat_piste` int(10) UNSIGNED NOT NULL,
  `nom_etat_piste` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_etat_piste`
--

INSERT INTO `t_etat_piste` (`id_etat_piste`, `nom_etat_piste`) VALUES
(1, 'Sava 1'),
(2, 'Sava 2'),
(3, 'Plan-Marmet'),
(4, 'Petiti-Marmet'),
(5, 'Les pointes'),
(6, 'Chasseral'),
(7, 'Le Rumont'),
(8, 'Le Fornel');

-- --------------------------------------------------------

--
-- Structure de la table `t_fnc_per`
--

CREATE TABLE `t_fnc_per` (
  `id_fnc` int(10) UNSIGNED NOT NULL,
  `id_per` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_fnc_per`
--

INSERT INTO `t_fnc_per` (`id_fnc`, `id_per`) VALUES
(1, 1),
(1, 2),
(1, 3),
(2, 1),
(2, 2),
(2, 3),
(3, 1),
(3, 2),
(3, 3),
(4, 1),
(4, 3);

-- --------------------------------------------------------

--
-- Structure de la table `t_fonctions`
--

CREATE TABLE `t_fonctions` (
  `id_fnc` int(10) UNSIGNED NOT NULL,
  `nom_fnc` varchar(100) NOT NULL,
  `abr_fnc` varchar(10) NOT NULL,
  `desc_fnc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_fonctions`
--

INSERT INTO `t_fonctions` (`id_fnc`, `nom_fnc`, `abr_fnc`, `desc_fnc`) VALUES
(1, 'Administrateur', 'Admin', 'Administrateur du site'),
(2, 'Utilisateur', 'User', 'Utilisateur du site'),
(3, 'Météo', 'Météo', 'Saisie de la météo'),
(4, 'ROOT', 'ROOT', 'Fonction avec toutes les autorisations');

-- --------------------------------------------------------

--
-- Structure de la table `t_personnes`
--

CREATE TABLE `t_personnes` (
  `id_per` int(10) UNSIGNED NOT NULL,
  `nom_per` varchar(30) NOT NULL,
  `prenom_per` varchar(30) NOT NULL,
  `email_per` tinytext NOT NULL,
  `password_per` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `t_personnes`
--

INSERT INTO `t_personnes` (`id_per`, `nom_per`, `prenom_per`, `email_per`, `password_per`) VALUES
(1, 'admin', 'admin', 'admin@ceff.ch', '$2y$10$ZPy9IX9aukEO6pmBJNJHBOpH4AaAhd02JmI5CflcrJO8iDK9q10dq'),
(2, 'user', 'user', 'user@ceff.ch', '$2y$10$koM9cfGemppdZsfMrOcoFOVZJF5LWW1I8hyHccCWklIMMu1mTmYQ.'),
(3, 'Norris', 'Chuck', 'norris@chuck.com', '$2y$10$GYPpa7LHjoNme.Jcxkb3d.cW9Me5Q/Y3VMbtAgnaQPCJOIavHxUNW'),
(4, 'admin', 'Secondaire', 'admin@secondaire.ch', '$2y$10$IJ574iDXUxaeq0d4LvkX0eCHhok06ZBmWQurNAr3B5a6HXaRr1THS'),
(5, 'Nom', 'prenom', 'vrai@email.com', '$2y$10$3MOlC/UHdT79P5H00K4wNON45bSqPjp.AuY1ubh3i4nZsagButBHm');

-- --------------------------------------------------------

--
-- Structure de la table `t_pistes`
--

CREATE TABLE `t_pistes` (
  `id_piste` int(10) UNSIGNED NOT NULL,
  `nom_piste` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_pistes`
--

INSERT INTO `t_pistes` (`id_piste`, `nom_piste`) VALUES
(1, 'Sava 1'),
(2, 'Sava 2'),
(3, 'Plan-Marmet'),
(4, 'Petiti-Marmet'),
(5, 'Les pointes'),
(6, 'Chasseral'),
(7, 'Le Rumont'),
(8, 'Le Fornel');

-- --------------------------------------------------------

--
-- Structure de la table `t_pistes_active`
--

CREATE TABLE `t_pistes_active` (
  `id_piste_active` int(10) UNSIGNED NOT NULL,
  `id_piste` int(11) NOT NULL,
  `date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_temps_meteo`
--

CREATE TABLE `t_temps_meteo` (
  `id_temps_meteo` int(10) UNSIGNED NOT NULL,
  `heure` text NOT NULL,
  `date` date NOT NULL,
  `etat_meteo` text NOT NULL,
  `etat_piste` int(11) NOT NULL,
  `etat_neige` int(11) NOT NULL,
  `commentaire` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `t_webcam`
--

CREATE TABLE `t_webcam` (
  `id_webcam` int(10) UNSIGNED NOT NULL,
  `url` varchar(30) NOT NULL,
  `nom` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `t_webcam`
--

INSERT INTO `t_webcam` (`id_webcam`, `url`, `nom`) VALUES
(1, '/photo/pmjour-', 'Plan-Marmet'),
(2, '/photo/ptsjours/pts1600-11-', 'Crete-de-Chasseral'),
(3, '/photo/ptsjours/pts1600-6-', 'Eolienne-Mont-Crosin'),
(4, '/photo/ptsjours/pts1600-3-', 'Val-de-Ruz'),
(5, '/photo/ptsjours/pts1600-4-', 'Joux-du-Plane'),
(6, '/photo/ptsjours/pts1600-5-', 'Piste-des-Pointes'),
(7, '/photo/ptsjours/pts1600-6-', 'Eolienne-Mont-Crosin');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `t_autorisations`
--
ALTER TABLE `t_autorisations`
  ADD PRIMARY KEY (`id_aut`);

--
-- Index pour la table `t_aut_fnc`
--
ALTER TABLE `t_aut_fnc`
  ADD PRIMARY KEY (`id_aut`,`id_fnc`),
  ADD UNIQUE KEY `id_aut` (`id_aut`,`id_fnc`),
  ADD KEY `id_fnc` (`id_fnc`);

--
-- Index pour la table `t_etat_meteo`
--
ALTER TABLE `t_etat_meteo`
  ADD PRIMARY KEY (`id_etat_meteo`);

--
-- Index pour la table `t_etat_neige`
--
ALTER TABLE `t_etat_neige`
  ADD PRIMARY KEY (`id_etat_neige`);

--
-- Index pour la table `t_etat_piste`
--
ALTER TABLE `t_etat_piste`
  ADD PRIMARY KEY (`id_etat_piste`);

--
-- Index pour la table `t_fnc_per`
--
ALTER TABLE `t_fnc_per`
  ADD UNIQUE KEY `id_fnc_2` (`id_fnc`,`id_per`),
  ADD KEY `id_fnc` (`id_fnc`),
  ADD KEY `id_per` (`id_per`);

--
-- Index pour la table `t_fonctions`
--
ALTER TABLE `t_fonctions`
  ADD PRIMARY KEY (`id_fnc`);

--
-- Index pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  ADD PRIMARY KEY (`id_per`),
  ADD KEY `id_per` (`id_per`);

--
-- Index pour la table `t_pistes`
--
ALTER TABLE `t_pistes`
  ADD PRIMARY KEY (`id_piste`);

--
-- Index pour la table `t_pistes_active`
--
ALTER TABLE `t_pistes_active`
  ADD PRIMARY KEY (`id_piste_active`);

--
-- Index pour la table `t_temps_meteo`
--
ALTER TABLE `t_temps_meteo`
  ADD PRIMARY KEY (`id_temps_meteo`);

--
-- Index pour la table `t_webcam`
--
ALTER TABLE `t_webcam`
  ADD PRIMARY KEY (`id_webcam`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `t_autorisations`
--
ALTER TABLE `t_autorisations`
  MODIFY `id_aut` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `t_etat_meteo`
--
ALTER TABLE `t_etat_meteo`
  MODIFY `id_etat_meteo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `t_etat_neige`
--
ALTER TABLE `t_etat_neige`
  MODIFY `id_etat_neige` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `t_etat_piste`
--
ALTER TABLE `t_etat_piste`
  MODIFY `id_etat_piste` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `t_fonctions`
--
ALTER TABLE `t_fonctions`
  MODIFY `id_fnc` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `t_personnes`
--
ALTER TABLE `t_personnes`
  MODIFY `id_per` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `t_pistes`
--
ALTER TABLE `t_pistes`
  MODIFY `id_piste` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `t_pistes_active`
--
ALTER TABLE `t_pistes_active`
  MODIFY `id_piste_active` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `t_temps_meteo`
--
ALTER TABLE `t_temps_meteo`
  MODIFY `id_temps_meteo` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `t_webcam`
--
ALTER TABLE `t_webcam`
  MODIFY `id_webcam` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `t_aut_fnc`
--
ALTER TABLE `t_aut_fnc`
  ADD CONSTRAINT `t_aut_fnc_ibfk_1` FOREIGN KEY (`id_aut`) REFERENCES `t_autorisations` (`id_aut`),
  ADD CONSTRAINT `t_aut_fnc_ibfk_2` FOREIGN KEY (`id_fnc`) REFERENCES `t_fonctions` (`id_fnc`);

--
-- Contraintes pour la table `t_fnc_per`
--
ALTER TABLE `t_fnc_per`
  ADD CONSTRAINT `t_fnc_per_fk_per` FOREIGN KEY (`id_per`) REFERENCES `t_personnes` (`id_per`),
  ADD CONSTRAINT `t_fnc_per_ibfk_1` FOREIGN KEY (`id_fnc`) REFERENCES `t_fonctions` (`id_fnc`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
