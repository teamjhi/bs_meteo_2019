<?php
require("../config/config.inc.php");
$aut = "USR_MTO;ADM_MTO";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
?>

<div class="row">
    <div class="header">
        <h1></h1>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            Page de modification de la météo
        </div>

        <div class="panel-body">

            <form id="meteo_form" method="post" novalidate="novalidate">

                <p>Le bouton ajouter vous permet d'entrer la météo du jour tandis que le bouton modifier vous permet de modifier les entrées saisies pour le jour correspondant. Tout les champs sont nécaissaires.</p>

                <!-- Date -->
                <div class="row form-group">
                    <div class="col-sm-12">

                        <div class="form-group" style="display: flex;">
                            <button class="input-group-prepend btn btn-link" type="button" data-toggle="tooltip" title="Un jour en arrière"><i class="glyphicon glyphicon-arrow-left"></i></button>
                            <input id="meteo_date_picker" type="text" class="form-control">
                            <button class="input-group-append btn btn-link" type="button" data-toggle="tooltip" title="Un jour en avant"><i class="glyphicon glyphicon-arrow-right"></i></button>
                        </div>

                    </div>
                </div>

                <!-- Heure -->
                <div class="row form-group">
                    <div class="col-sm-2">
                        <label for="heure_bul" class="col-form-label">Heure</label>
                    </div>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <!--div id="time_picker" class="input-group">
                                <input type="text" class="form-control">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div-->
                            <div id="time_picker" class="input-group">
                                <input class="form-control" type="time" value="00:00" id="timer-picker-input">
                                <span class="input-group-addon">H</span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Température -->
                <div class="row form-group">
                    <div class="col-sm-2">
                        <label for="temperature_bul" class="col-form-label">Température</label>
                    </div>
                    <div class="col-sm-10">
                        <div id="heat_picker" class="input-group date">
                            <input type="number" value="0" class="form-control valid" id="temperature_bul" name="temperature_bul" aria-invalid="false">
                            <span class="input-group-addon">
                                    °C
                            </span>
                        </div>
                    </div>
                </div>

                <!-- Météo -->
                <div class="row form-group">
                    <div class="col-sm-2">
                        <label for="temps_meteo" class="col-form-label">Etat de la meteo</label>
                    </div>
                    <div class="col-sm-10">
                        <select class="form-control valid" id="temps_meteo" name="temps_meteo" aria-invalid="false">

                            <option disabled="" selected="" value="">Etat de la meteo</option>
                            <option value="ensoleille">Ensoleillé</option>
                            <option value="partiellement_nuageux">Partiellement nuageux</option>
                            <option value="nuageux">Nuageux</option>
                            <option value="petite_neige">Peu de neige</option>
                            <option value="grande_neige">Beacoup de neige</option>
                        </select>
                    </div>
                </div>

                <!-- Etat des pistes -->
                <div class="row form-group">
                    <div class="col-sm-2">
                        <label for="etat_piste" class="col-form-label">Pistes</label>
                    </div>
                    <div class="col-sm-10">
                        <select class="form-control valid" id="etat_piste" name="etat_piste" aria-invalid="false">
                            <option disabled="" selected="" value="">Etat des pistes</option>
                            <option value="tres_mauvaise">Très mauvaises</option>
                            <option value="mauvaises">Mauvaises</option>
                            <option value="moyennes">Moyennes</option>
                            <option value="bonnes">Bonnes</option>
                            <option value="tres bonnes">Très bonnes</option>
                        </select>
                    </div>
                </div>

                <!-- Etat de la neige -->
                <div class="row form-group">
                    <div class="col-sm-2 offset-sm-3">
                        <label for="etat_neige" class="col-form-label">Neige</label>
                    </div>
                    <div class="col-sm-10">
                        <select class="form-control valid" id="etat_neige" name="etat_neige" aria-invalid="false">
                            <option disabled="" selected="" value="">Etat de la neige</option>
                            <option value="dure">Dure</option>
                            <option value="normal">Normal</option>
                            <option value="fine">Fine</option>
                            <option value="poudreuse">Poudreuse</option>
                        </select>
                    </div>
                </div>

                <!-- Boutons -->
                <div class="row form-group action-button">
                    <input type="submit" class="btn btn-primary submit" id="butt_conf" name="butt_conf" value="Ajouter">
                    <input type="reset" class="btn btn-warning submit" id="butt_cancel" name="butt_cancel" value="Annuler">
                </div>
            </form>

        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>

</div>

<script src="js/index.js"></script>
</body>

</html>