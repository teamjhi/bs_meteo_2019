$(function(){

    // Active le champs de selection de la date //https://bootstrap-datepicker.readthedocs.io/en/latest/i18n.html
    $('#meteo_date_picker').datepicker({
        todayBtn: "linked",
        format: 'dd/mm/yyyy',
        language: "fr",
        autoclose: true,
        todayHighlight: true
    });

    // Met le champ formulaire au jour d'aujourd'hui
    $('#meteo_date_picker').datepicker('update', new Date());

    // Empêche les gens de cliquer dessus
    $('#meteo_date_picker').datepicker()
        .on("show", function(e) {
            $('#meteo_date_picker').datepicker('hide');
    });

    /* Time selection
    var defaults = {
        calendarWeeks: true,
        showClear: true,
        showClose: true,
        allowInputToggle: true,
        useCurrent: false,
        ignoreReadonly: true,
        minDate: new Date(),
        toolbarPlacement: 'top',
        locale: 'nl',
        icons: {
            time: 'fa fa-clock-o',
            date: 'fa fa-calendar',
            up: 'fa fa-angle-up',
            down: 'fa fa-angle-down',
            previous: 'fa fa-angle-left',
            next: 'fa fa-angle-right',
            today: 'fa fa-dot-circle-o',
            clear: 'fa fa-trash',
            close: 'fa fa-times'
        }
    };
    var optionsTime = $.extend({}, defaults, {format:'HH:mm'});
    $('#time_picker').datetimepicker(optionsTime);*/

});