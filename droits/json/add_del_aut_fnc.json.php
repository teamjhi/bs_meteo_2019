<?php
header('Content-Type: application/json');
session_start();
require("../../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$id_aut = $_POST["id_aut"];
$id_fnc = $_POST["id_fnc"];
$status = $_POST["status"];
$id_auth = $_POST["id_auth"];

$autorisation = new Autorisation($id_aut);
$tab["id_auth"] = $id_auth;

sleep(0.666);

if($status === "true") {

    $tab["reponse"] = $autorisation->add_fnc($id_fnc);
    $tab["operation"] = "add";
}else {

    $tab["reponse"] = $autorisation->del_fnc($id_fnc);
    $tab["operation"] = "del";
}

if($tab["reponse"]){

    $tab["message"]["type"] = "success";

    if($tab["operation"] === "add") {

        $tab["message"]["texte"] = "L'ajout de la fonction a bien été enrengistré";
    }else {

        $tab["message"]["texte"] = "La suppression de la fonction a bien été enrengistré";
    }
}else {

    $tab["message"]["type"] = "danger";

    if($tab["operation"] === "add") {

        $tab["message"]["texte"] = "L'ajout de la fonction n'a pas pu se faire";
    }else {

        $tab["message"]["texte"] = "La suppression de la fonction n'a pas pu se faire";
    }
}

echo json_encode($tab);