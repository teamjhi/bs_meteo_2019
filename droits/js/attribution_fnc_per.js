$(function(){

    $(".auth").click(function(){

        //$("#loading").css("display", "block");
        $(this).css("display", "none");
        $(this).parent().append("<img class=\"loading\" src=\"./../icones/loading.gif\" height=38px>");

        $.post(
            "./json/add_del_fnc_per.json.php?_=" + Date.now(),
            {
                id_per: $(this).attr("id_per"),
                id_fnc: $(this).attr("id_fnc"),
                status: $(this).is(":checked"),
                id_auth: $(this).attr("id")
            },
            function (data, status){

                var background = data.operation === "add" ? " #90ee97": "#ee9090";
                $("#"+data.id_auth).parent().css("background", background);

                message(data.message.texte, data.message.type);

                $("#"+data.id_auth).siblings(".loading").remove();
                $("#"+data.id_auth).css("display", "block");

                //$("#loading").css("display", "none");
            }
        );

    });

});