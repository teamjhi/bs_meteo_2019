<?php
require("../config/config.inc.php");
$aut = "USR_MTO;ADM_MTO";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
?>

<div class="row">
    <div class="header">
        <h1></h1>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            Page de modification de l'état des pistes
        </div>

        <div class="panel-body">

            <form id="installations_form" method="post">

                <p>Le champ de selection de date vous permets d'éditer l'état des piste pour chaque jour, en vert sont les installations ouvertes.</p>

                <!-- Date -->
                <div class="row form-group">
                    <div class="col-sm-12">

                        <div class="form-group" style="display: flex;">
                            <button class="input-group-prepend btn btn-link" type="button" data-toggle="tooltip" title="Un jour en arrière"><i class="glyphicon glyphicon-arrow-left"></i></button>
                            <input id="pistes_date_picker" type="text" class="form-control">
                            <button class="input-group-append btn btn-link" type="button" data-toggle="tooltip" title="Un jour en avant"><i class="glyphicon glyphicon-arrow-right"></i></button>
                        </div>

                    </div>
                </div>

                <div class="list-group list-pistes">
                    <a href="#" class="list-group-item list-group-item-action list-group-item-secondary">
                        <span class="badge badge-success"> </span>
                        <span>Sava 1</span>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action list-group-item-secondary">
                        <span class="badge badge-danger"> </span>
                        <span>Sava 2</span>
                    </a>
                    <a href="#" class="list-group-item list-group-item-action list-group-item-secondary">
                        <span class="badge badge-danger"> </span>
                        <span>Plan-Marmet</span>
                    </a>

                    <a href="#" class="list-group-item list-group-item-action list-group-item-secondary">
                        <span class="badge badge-success"> </span>
                        <span>Petiti-Marmet</span>
                    </a>

                    <a href="#" class="list-group-item list-group-item-action list-group-item-secondary">
                        <span class="badge badge-danger"> </span>
                        <span>Les Pointes</span>
                    </a>

                    <a href="#" class="list-group-item list-group-item-action list-group-item-secondary">
                        <span class="badge badge-success"> </span>
                        <span>Chasseral</span>
                    </a>

                    <a href="#" class="list-group-item list-group-item-action list-group-item-secondary">
                        <span class="badge badge-success"> </span>
                        <span>Le Rumont</span>
                    </a>

                    <a href="#" class="list-group-item list-group-item-action list-group-item-secondary">
                        <span class="badge badge-success"> </span>
                        <span>Le fornel</span>
                    </a>
                </div>

            </form>

        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>

</div>

<script src="js/index.js"></script>
</body>

</html>