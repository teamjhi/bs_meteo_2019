$(function(){

    // Active le champs de selection de la date //https://bootstrap-datepicker.readthedocs.io/en/latest/i18n.html
    $('#pistes_date_picker').datepicker({
        todayBtn: "linked",
        format: 'dd/mm/yyyy',
        language: "fr",
        autoclose: true,
        todayHighlight: true
    });

    // Met le champ formulaire au jour d'aujourd'hui
    $('#pistes_date_picker').datepicker('update', new Date());

    // Empêche les gens de cliquer dessus
    $('#pistes_date_picker').datepicker()
        .on("show", function(e) {
            $('#pistes_date_picker').datepicker('hide');
        });

});