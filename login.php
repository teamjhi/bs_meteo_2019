<?php
require("./config/config.inc.php");
require("./includes/head.inc.php");
?>

<div class="row">
    <div class="header">
        <h1>Connexion</h1>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            Connexion au portail d'administration des Bugnenets-Savagnière SA
        </div>

        <div class="panel-body">

            <p>
                <img src="./source/images/logo.png">
            </p>

            <p>
                Bienvenue au portail d'administration des Bugnenets-Savagnière SA, veuillez vous identifiez sur la platforme affin d'accéder au tableau de bord. Si vous n'êtes plus en mesure de vous connecter, merci de regarder avec votre administreateur local après avoir essayer à nouveau si le problème est persistant.
            </p>

            <form id="login_form" action="check.php" method="post">

                <!-- Email -->
                <div class="form-group row">
                    <label for="email_per" class="col-sm-2 col-form-label">E-mail</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="email_per" name="email_per" placeholder="Votre adresse e-mail">
                    </div>
                </div>

                <!-- Mot de passe -->
                <div class="form-group row">
                    <label for="password" class="col-sm-2 col-form-label">Mot de passe</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Votre mot de passe">
                    </div>
                </div>

                <!-- Groupe de boutton -->
                <div class="form-group action-button">
                    <input type="submit" class="btn btn-primary" value="Se connecter">
                    <a href="login.php" role="button" class="btn btn-warning">Annuler</a>
                </div>

            </form>
        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>

</div>
<script src="js/function.js"></script>
<script src="./js/login.js"></script>
</body>

</html>