<?php
require("./config/config.inc.php");
$aut = "ADM_USR";
require(WAY . "/includes/secure.inc.php");
require_once(WAY . "/includes/head.inc.php");
?>

<div class="row">
    <div class="header">
        <h1>Bienvenue</h1>
    </div>
</div>

<div class="col-md-12">
    <div class="panel panel-primary">

        <div class="panel-heading">
            Portail d'administration des Bugnenets-Savagnière SA
        </div>

        <div class="panel-body">

            <p>
                <img src="./source/images/bs002.jpg">
            </p>

            <h3>Foire aux questions</h3>

            <div class="accordion" id="accordionFaq">
                <div class="card">
                    <div class="card-header" id="accordion-heading-one">
                        <h2 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Securité
                            </button>
                        </h2>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionFaq">
                        <div class="card-body">
                            La page de securité vous donne accès aux fonctions essentiels telle que l'attribution des rôles qui permet d'attribuer une fonction ou un rôle à un utilisateurs qui aura des authorisations sembable à des droits pour chaque rôle.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="accordion-heading-two">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Accès
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionFaq">
                        <div class="card-body">
                            La page accès vous permet de vous connecter ou de vous déconnecter simplement.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="accordion-heading-three">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Gestion
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionFaq">
                        <div class="card-body">
                            La page gestion vous permets de gérer l'état des pistes ainsi que l'affichage de la météo.
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="accordion-heading-four">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                Elements
                            </button>
                        </h2>
                    </div>
                    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionFaq">
                        <div class="card-body">
                            Chaque page se présente avec une sécurité, l'accès aux pages dépends des utilisateurs, il est possible de contacter votre administrateur pour toute questions.
                        </div>
                    </div>
                </div>
            </div>


        </div>

        <div class="panel-footer">

        </div>

    </div>
</div>
