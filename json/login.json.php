<?php
header('Content-Type: application/json');
session_start();
require("./../config/config.inc.php");
require_once(WAY . "/includes/autoload.inc.php");

$per = new Personne();

// Regarde si la combinaison email et mot de passe existe
if($per->check_login($_POST['email_per'], $_POST['password'])) {

    $tab['reponse'] = true;
    $tab['message']['texte'] = "Connexion valide.";
    $tab['message']['type'] = "success";

}else {

    $id = $per->add($_POST);
    $per->set_id($id);

    if($per->init()) {

        $tab['response'] = true;
        $tab['message']['texte'] = "Connexion invalide !";
        $tab['message']['type'] = "danger";
    }

}

echo json_encode($tab);
?><?php
