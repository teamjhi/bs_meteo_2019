<!doctype html>
<html lang="fr">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>BS-Master 2000</title>

    <!-- CSS utilisée sur tout le site -->
    <link rel="stylesheet" href="<?= URL ?>/css/global.css">

    <!-- CSS de bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://bootswatch.com/3/simplex/bootstrap.min.css" crossorigin="anonymous">

    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>

    <!-- Bootstrap -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Jquery validation plugin -->
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>

    <!-- Popper -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <!-- Font awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css" rel="stylesheet">

    <!-- Moment JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/locale/fr.js"></script>

    <!-- Bootstrap toggle -->
    <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/css/bootstrap4-toggle.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.4.0/js/bootstrap4-toggle.min.js"></script>

    <!-- Bootstrap datepicker -->
    <link href="<?= URL ?>/source/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    <script src="<?= URL ?>/source/js/bootstrap-datepicker.min.js"></script>

    <!-- Bootstrap timepicker -->
    <link href="<?= URL ?>/source/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <script src="<?= URL ?>/source/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Fonction JS -->
    <script src="<?= URL ?>/js/function.js"></script>

    <!-- Script global -->
    <script src="<?= URL ?>/js/global.js"></script>

</head>

<body>

    <div class="container">

        <div id="loading">
            <img id="loading_img" src="<?= URL ?>/icones/loading.gif">
        </div>

        <!-- Zone de notification -->
        <div class="alert" id="alert">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong class="bold"></strong><span class="message"></span>
        </div>
        <?php if(isset($_SESSION['id'])){ ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <a class="navbar-brand" href="<?php echo URL;?>">BS-Master 2000</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Securité <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="<?= URL ?>/droits/attribution_fnc_per.php">Attribution des rôles</a></li>
                                    <li><a class="dropdown-item" href="<?= URL ?>/droits/attribution_aut_fnc.php">Attribution des droits</a></li>
                                    <li><a class="dropdown-item" href="<?= URL ?>/droits/fonctions.php">Rôles utilisateur</a></li>
                                    <li><a class="dropdown-item" href="<?= URL ?>/droits/autorisations.php">Droits d'accès</a></li>
                                </ul>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gestion <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="<?= URL ?>/meteo/index.php">Météo</a></li>
                                    <li><a class="dropdown-item" href="<?= URL ?>/pistes/index.php">Pistes</a></li>
                                </ul>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Utilisateurs <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="<?= URL ?>/utilisateurs/inscription.php">Inscription</a></li>
                                </ul>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Accès <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="<?= URL ?>/logout.php">Déconnection</a></li>
                                </ul>
                            </li>
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                        <li><a href="<?= URL?>/logout.php">X</a></li>
                    </ul>
                    </div>
                </div>
            </nav>
        <?php } ?>